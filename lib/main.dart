import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState ();
}

class _MyAppState extends State<MyApp> {
  double _width = 100;
  double _height = 100;
  double _updateState() {

    setState(() {
      _width = 300;
      _height = 300;

    });
  }

  @override
  Widget build(BuildContext context) {
      return MaterialApp(
        title: 'Animation',
        home: Scaffold(
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                  onPressed: () {
                    _updateState();
                  },
                  child: Text('Click'),
                ),
                AnimatedContainer(
                  duration: Duration(seconds: 3),
                  curve: Curves.easeInOutQuint,
                  width: _width,
                  height: _height,
                  color: Colors.greenAccent[200],
                  child: Center(
                    child: Text('Animation'),
                  ),
                )
              ],
            )

          )
        )
      );
  }




}